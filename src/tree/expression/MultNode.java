/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.expression;

/**
 *
 * @author Eduardo
 */
public class MultNode extends BinaryOperatorNode{

    public MultNode(ExpressionNode raito, ExpressionNode leftou) {
        super(raito, leftou);
    }

    @Override
    public float evaluate() {
        return leftou.evaluate()*raito.evaluate();
    }
    
}
